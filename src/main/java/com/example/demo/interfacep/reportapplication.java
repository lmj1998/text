package com.example.demo.interfacep;

import com.example.demo.dao.admin;
import com.example.demo.dao.report;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface reportapplication extends JpaRepository<report,Integer> {
    public List<report> findByTime(Date time);
}
